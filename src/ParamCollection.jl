abstract type AbstractParameterCollection <: AbstractVector{AbstractParameter} end

struct ParamCollection <: AbstractParameterCollection
    parameters::Vector{AbstractParameter}
end


getindex(p::AbstractParameterCollection, args...) = getindex(p.parameters, args...)
getindex(p::AbstractParameterCollection, r::AbstractRange) = typeof(p)(getindex(p.parameters, r))
getindex(p::AbstractParameterCollection, ::Colon, j::Integer) = [getindex(i.param, j) for i in p]
getindex(p::AbstractParameterCollection, ::Colon, s::Symbol) = [getindex(i, s) for i in p]
# getindex(p::AbstractParameterCollection, j::Integer, ::Colon) = [getindex(i.param, j) for i in p]
getindex(p::AbstractParameterCollection, j::Integer, ::Colon) = [getindex(i.param, j) for i in p]
getindex(p::AbstractParameterCollection, ::Colon, ::Colon) = vcat([i.param' for i in p]...)
getindex(p::AbstractParameterCollection, r1::AbstractArray, r2::AbstractArray) = vcat([p[i].param[r1]' for i in r2]...)
# getindex(p::AbstractParameterCollection, r::AbstractUnitRange, ::Colon) = vcat([p[i].param' for i in r]...)
# getindex(p::AbstractParameterCollection, j::Integer, r::AbstractUnitRange) = [getindex(p[i].param, j) for i in r]

getindex(p::AbstractParameterCollection, i::Integer, j::Integer )= getindex(p[i].param, j)

function getindex(p::AbstractParameterCollection, s::Symbol) 
    hasfield(typeof(first(p)), s) && return getfield.(p, s)
    hasfield(typeof(first(p).model), :params) && return [param[s] for param in p]

    error(":$s is neither a parameter type field nor a parameter name. \n
    Valid fields are: $(fieldnames(typeof(first(p))))\n
    Valid parameter names are: $(first(p).model.params)")
end


for func in (:setindex!, :length, :lastindex, :size, :iterate)
    @eval Base.$func(p::AbstractParameterCollection, args...) = Base.$func(p.parameters, args...)
end

function push!(pars::AbstractParameterCollection, p::AbstractParameter; verbose=true)
    if p in pars
        verbose && @warn "Parameter already in ParameterCollection, ignoring push! operation."
    else
        push!(pars.parameters, p)
    end
    return nothing
end


function append!(pars::AbstractParameterCollection, pars2::AbstractParameterCollection)
    ### This is inefficient, but it does ensure that we do not duplicate params.
    for p in pars2
        push!(pars, p; verbose=false)
    end
end

function merge(p::AbstractParameterCollection, p2::AbstractParameterCollection)
    return typeof(p)([p.parameters; p2.parameters])
end



function Base.show(io::IO, p::AbstractParameterCollection)
    println(io, "Parameter Collection")
    for (i, p) in enumerate(p.parameters)
        println(io, "$i $(typeof(p.model)) : $(p.param)")
    end
end

function Base.show(io::IO, ::MIME"text/plain", p::AbstractParameterCollection)
    println(io, "Parameter Collection with $(length(p)) parameter sets.")
    if length(p.parameters) < 12
        for (i, p) in enumerate(p.parameters)
            println(io, "$i: $(p.param) for model: $(typeof(p.model))")
        end
    else
        for i in 1:5
            println(io, "$i: $(p[i].param) for model: $(typeof(p[i].model))")
        end
        println(io, "...")
        l = length(p.parameters)
        for i in l-5:l
            println(io, "$i: $(p[i].param) for model: $(typeof(p[i].model))")
        end

    end
end
