@latexrecipe function f(p::AbstractParameter)
    if p.model isa DiffEqBase.AbstractParameterizedFunction || p.model isa DiffEqBase.AbstractReactionNetwork
        env --> :mdtable
        transpose --> true
        return p.model.params, p
    end
    return p.param
end
