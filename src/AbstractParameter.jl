abstract type AbstractParameter{T} <: AbstractVector{T} end

for func in (:getindex, :setindex!, :length, :lastindex, :size, :iterate)
    @eval Base.$func(p::AbstractParameter, args...) = Base.$func(p.param, args...)
end


function Base.getindex(p::AbstractParameter, s::Symbol)
    ind = findfirst(isequal(s), p.model.params)
    errmsg = "The key you provided does not exist.
Valid keys are: $(join(p.model.params, ", "))."
    @assert ind != nothing errmsg
    return p[ind]
end

function setindex!(p::AbstractParameter, val, s::Symbol)
    ind = findfirst(isequal(s), p.model.params)
    errmsg = "The key you provided does not exist.
Valid keys are: $(join(p.model.params, ", "))."
    @assert ind != nothing errmsg
    return setindex!(p, val, ind)
end

function ==(p1::AbstractParameter, p2::AbstractParameter)
    fieldnames(typeof(p1)) != fieldnames(typeof(p2)) && return false
    for field in fieldnames(typeof(p1))
        getfield(p1, field) != getfield(p2, field) && return false
    end
    return true
end

rightjust(s, width) = " "^max(0, width - length(string(s))) * string(s)
leftjust(s, width) = string(s) * " "^max(0, width - length(string(s)))

function Base.show(io::IO, ::MIME"text/plain", p::AbstractParameter)
    println(io, "$(length(p.param))-element $(typeof(p)):")
    if :params in fieldnames(typeof(p.model))
        longest_name = maximum(length.(string.(p.model.params)))

        for i in eachindex(p.param)
            println(io, "  $(leftjust(p.model.params[i], longest_name)) = $(p[i])")
        end
    else
        width = length(string(length(p.param))) + 1
        for i in eachindex(p.param)
            println(io, "$(leftjust(string(i)*':', width)) $(p[i])")
        end
    end
end
# function reset!(p::AbstractParameter)
#     if :original_param in fieldnames(p)
#         p.param .= p.original_param
#     else
#         @warn "reset!() only works if your parameter type has a field `original_params`. See the Docs."
#     end
#     nothing
# end
