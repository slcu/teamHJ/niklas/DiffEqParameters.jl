
(p::ParamCollection)(ode::DiffEqBase.AbstractReactionNetwork) = ParamCollection([i for i in p if ode isa typeof(i.model)])
# (p::ParamCollection)(ode::DiffEqBase.AbstractReactionNetwork, cost::Float64) = (p::ParamCollection)(ode, (-Inf, cost))
(p::ParamCollection)(f::Function) = ParamCollection([i for i in p if f isa typeof(i.model)])

function (p::ParamCollection)(ode::DiffEqBase.AbstractReactionNetwork, cost::Tuple{Float64, Float64})
    @warn("Deprecated method, use @filter instead.")
    ParamCollection([i for i in p if ode isa i.modeltype && cost[1] <= i.cost <= cost[2]])
end


macro filter(pc, cond)
    cond = fieldcond(cond)
    # cond = Meta.parse(join(string.(cond), " "))
    return :($typeof($(esc(pc)))([_p for _p in $(esc(pc)) if $cond]))
    # return :($(esc(typeof(pc)([_p for _p in pc if cond]))))
end

# function new_filtered(mod, pc, cond)
#     ex = :($typeof($(pc))([_p for _p in $(pc) if $cond]))
#     # println(ex)
#     # return Core.eval(mod, ex)
#     return ex
# end

function fieldcond(cond)
    return MacroTools.postwalk(x-> x isa Symbol ? :(Symbol($"$x") in fieldnames(typeof(_p)) ? getfield(_p, Symbol($"$x")) : $(esc(x))) : x, cond)
end

# function fieldcond(pc::AbstractParameterCollection, t::Tuple)
#     conds = collect(t)
#     for i in eachindex(conds)
#         conds[i] = fieldcond(pc, conds[i])
#     end
#     return conds
# end
#
# fieldcond(pc::AbstractParameterCollection, s::Symbol) = s
# function fieldcond(pc::AbstractParameterCollection, c::Expr)
#     fields = get_fieldnames(pc)
#     c = MacroTools.postwalk(x-> x in fields ? :(getfield(_p, Symbol($"$x"))) : x, c)
#     return c
# end

# get_fieldnames(pc::AbstractParameterCollection) = Set(Iterators.flatten(fieldnames.(typeof.(pc))))

# cost(par::AbstractParameterCollection) = getfield.(par, :cost)
# loss(par::AbstractParameterCollection) = [p.metadata[:cost][:loss] for p in par]

# function best_param(p, ode)
#     minind = 1
#     mincost = Inf
#     for i in 1:length(p)
#         if !ismissing(p[i].cost) & p[i].cost < mincost
#             minind = copy(i)
#             mincost = copy(p[i].cost)
#         end
#     end
#     return p[minind]
# end


# function filter_params(par::AbstractParameterCollection, ode::ODEType;
#     cost_min=-Inf, cost_max=Inf, loss=:any, tshift=:any, pamp=:any)
#
#     filtered_param = ParamCollection([p for p in par if
#             ode isa typeof(p.model) &&
#             cost_min <= p.cost <= cost_max &&
#             (loss == :any || loss == p.metadata[:cost][:loss] ) &&
#             (tshift == :any || tshift == p.tshift) &&
#             (pamp == :any || pamp == p.metadata[:cost][:pamp] )
#         ])
#     return filtered_param
# end

param_matrix(par) = hcat((p for p in par)...)
