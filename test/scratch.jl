using Revise
revise()
using DiffEqParameters
using DiffEqBiological
using ParameterizedFunctions
using Test

f(x, p) = x .* p
p1 = Param(f, [1.])

typeof(f)
f isa Function

rn = @reaction_network MyRN begin
    r_1, 0 --> X
    d, X --> 0
end r_1 d

rnp1 = Param(rn, [1., 2.])

rnp2 = Param(rn, [1.]) # This should error
@test_throws AssertionError Param(rn, [1.])

ode = @ode_def MyODE begin
    dx = r_1 - d_x*x
end r_1 d_x

odep1 = Param(ode, [2.,3.])
odep2 = Param(ode, [1.,2.])
odep3 = Param(ode, [1.,2., 2]) # should error

pc1 = ParamCollection([p1, rnp1, rnp1])
pc2 = ParamCollection([odep1, odep2])

pc = merge(pc1, pc2)

pc(rn)[:, :d]

rnp1[:r_1]

@ParamType MyParam3 myfield::String field2 field3::Number

m = MyParam3(rn, [1,2], "hello", 1, 3)

m.field3

m

push!(pc, m)

pc

using DiffEqParameters
model1(x, p) = p[1]*x^p[2] ## A pretty useless model, but you get the point.
param_values = [10., 2.] ## A normal array.
param1 = Param(model1, param_values )
show(param1)

param1


@ParamType CostParam cost::Number
pc = ParamCollection([
    CostParam(rn, [1.,4], 0.9),
    CostParam(rn, [2.,5], 0.8),
    CostParam(rn, [3.,8], 1.9),
    ])

# macro myfilter(pc::AbstractParameterCollection, cond)

using MacroTools
macro filter(pc, cond)
    fields = fieldnames(typeof(pc[1]))
    cond = MacroTools.postwalk(x-> x in fields ? :(getfield(_p, Symbol($"$x"))) : x, cond)
    ex = :( $typeof($pc)([_p for _p in $pc if $(cond)] ))
    return ex
end

revise()
@filter(pc, 0.85<cost<1)
@filter(pc, cost<=0.9)
@filter(pc, 0<cost)

cond = :(0<cost<100)
cond.args


cond = MacroTools.postwalk(x-> x in fields ? getfield(pc[1], x) : x, cond)

fields = fieldnames(typeof(pc[1]))

[field for field in fieldnames(typeof(pc[1])) if field in cond.args]
fieldnames(typeof(pc[1]))

isdefined(Main, cond.args)
if eval(:(params[1]>0))
    print("hello")
end
